while (!isHappy()) {
    giveACookie();
}

// The code inside the {} will be executed while the condition is true
// In this example: while isHappy() returns false, we give a cookie

do {
    giveACookie();
} while (!isHappy());

// The do/while is the same, but the code will always be executed the first time
// With a while, the code is executed only if the condition is true