'I want to write
on several lines'
// Error: Strings can be written on one line only

'I want to write \
on several lines'
// No error because we escape the line break, but it won't add a line break in the string

'I want to write\n on several lines'
// This works (but is not easy to read): \n character means: add a break line here

`This is a string
with break lines
and I don't need \n`
// A better solution is to use ` delimiter
