import React from 'react';
import { Heading, Slide, CodePane, List, ListItem } from 'spectacle';

export default class ValueTypes extends React.Component {
  render() {
    const source = require('raw-loader!../code/typeof.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Value types
        </Heading>

        <List textColor={'primary'}>
          <ListItem textSize={30} height={45}><strong>String</strong> "hello world"</ListItem>
          <ListItem textSize={30} height={45}><strong>Number</strong> 5, 1.33</ListItem>
          <ListItem textSize={30} height={45}><strong>Function</strong> console.log, setTimeout</ListItem>
          <ListItem textSize={30} height={45}><strong>Boolean</strong> true, false</ListItem>
          <ListItem textSize={30} height={45}><strong>Undefined</strong></ListItem>
          <ListItem textSize={30} height={45}><strong>Object</strong> Math, window, Array</ListItem>
        </List>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
