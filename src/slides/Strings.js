import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class Strings extends React.Component {
  render() {
    const source = require('raw-loader!../code/strings-delimiter.txt');
    const color = {color: '#c311aa'};

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Strings
        </Heading>

        <Text textColor={'primary'} fit>
          Strings are delimited by quotes.<br />
          They must begin and end with the same delimiter.<br />
          String delimiters are <strong style={color}>"</strong>, <strong style={color}>'</strong>, <strong style={color}>`</strong>
        </Text>

        <CodePane lang={'js'} source={source} margin={'20px 0'} />
      </Slide>
    );
  }
}
