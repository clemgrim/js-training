import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class Math extends React.Component {
  render() {
    const source = require('raw-loader!../code/math.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Math
        </Heading>

        <Text textColor={'primary'} fit>
          Math is an <strong>object</strong> containing functions/constants related to math
        </Text>

        <CodePane lang={'js'} source={source} margin={'20px 0'} />
      </Slide>
    );
  }
}
