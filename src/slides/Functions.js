import React from 'react';
import { Heading, Slide, CodePane } from 'spectacle';

export default class Functions extends React.Component {
  render() {
    const source = require('raw-loader!../code/functions.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Functions
        </Heading>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
