import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class StringsQuotes extends React.Component {
  render() {
    const source = require('raw-loader!../code/strings-quotes.txt');
    const color = {color: '#c311aa'};

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Strings - Quotes
        </Heading>

        <CodePane lang={'js'} source={source} margin={'20px 0'} />

        <Text textColor={'primary'} fit>
          Conclusion: It is better to always use <strong style={color}>`</strong> to avoid quote issues
        </Text>
      </Slide>
    );
  }
}
