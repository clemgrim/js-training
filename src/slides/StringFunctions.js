import React from 'react';
import { Heading, Slide, CodePane } from 'spectacle';
import Text from 'spectacle/es/components/text';

export default class StringFunctions extends React.Component {
  render() {
    const source = require('raw-loader!../code/string-functions.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          String - Functions
        </Heading>

        <Text textColor={'primary'} fit>
          These functions don't modify the string, they return a new one.
        </Text>

        <CodePane lang={'js'} source={source} margin={'20px 0'} />
      </Slide>
    );
  }
}
