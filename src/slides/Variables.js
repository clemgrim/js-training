import React from 'react';
import { Heading, Slide, Text, CodePane } from 'spectacle';

export default class Variables extends React.Component {
  render() {
    const source = require('raw-loader!../code/variables.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Variables
        </Heading>

        <Text textColor={'primary'} fit>
          Variables are used to label and store a value in memory
        </Text>

        <CodePane lang={'js'} source={source} margin={'2rem 0 0'} />
      </Slide>
    );
  }
}
