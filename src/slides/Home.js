import React from 'react';
import { Heading, Slide, Text } from 'spectacle';

export default class Home extends React.Component {
  render() {
    return (
      <Slide bgColor="tertiary">
        <Heading size={1} fit caps lineHeight={1} textColor="secondary">
          Hosco Training
        </Heading>
        <Text margin="10px 0 0" textColor="primary" size={1} bold>
          introduction to javascript
        </Text>
      </Slide>
    );
  }
}
