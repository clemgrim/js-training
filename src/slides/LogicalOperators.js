import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class LogicalOperators extends React.Component {
  render() {
    const source = require('raw-loader!../code/logical-operators.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Logical Operators
        </Heading>

        <Text textColor={'primary'} fit>
          Logical operators are used to determine the logic between variables or values.
        </Text>

        <CodePane lang={'js'} source={source} margin={'2rem 0 0'} />
      </Slide>
    );
  }
}
