import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class ArrowFunctions extends React.Component {
  render() {
    const source = require('raw-loader!../code/arrow-functions.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Arrow Functions
        </Heading>

        <Text textColor={'primary'} fit>
          Arrow functions are a more concise syntax for writing function
        </Text>

        <CodePane lang={'js'} source={source} margin={'20px 0'}/>
      </Slide>
    );
  }
}
