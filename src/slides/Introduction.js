import React from 'react';
import { Heading, Slide, Image } from 'spectacle';

export default class Introduction extends React.Component {
  render() {
    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Introduction
        </Heading>

        <Image src={require('../assets/js-lang.png')} width={300} />

        <Image src={require('../assets/browsers.png')} margin={'40px auto 0'} width={300}/>
      </Slide>
    );
  }
}
