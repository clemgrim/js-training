import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import CodePane from "spectacle/es/components/code-pane";

export default class Boolean extends React.Component {
  render() {
    const source = require('raw-loader!../code/boolean.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Boolean
        </Heading>

        <Text textColor={'primary'} fit>
          A Boolean is a value that can be either <strong>true</strong> or <strong>false</strong>.
        </Text>

        <Text textColor={'primary'} fit margin={'1rem 0'}>
          Booleans are used to contain the result of a <strong>comparison</strong> or a <strong>logical</strong> operation.
        </Text>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
