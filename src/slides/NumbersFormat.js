import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class NumbersFormat extends React.Component {
  render() {
    const source = require('raw-loader!../code/numbers-format.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1} fit>
          Numbers - Format
        </Heading>

        <Text textColor={'primary'} fit>
          We use <strong>Number.prototype.toFixed()</strong> function to format a number
        </Text>

        <CodePane lang={'js'} source={source} margin={'20px 0'} />
      </Slide>
    );
  }
}
