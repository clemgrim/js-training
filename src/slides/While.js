import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import CodePane from "spectacle/es/components/code-pane";

export default class While extends React.Component {
  render() {
    const source = require('raw-loader!../code/while.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          While loop
        </Heading>

        <Text textColor={'primary'} fit margin={'20px 0'}>
          The while loop is another kind of loop, that is used when you don't need indexes
        </Text>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
