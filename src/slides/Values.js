import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class Values extends React.Component {
  render() {
    const source = require('raw-loader!../code/values.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Values
        </Heading>

        <Text textColor={'primary'} fit>
          A value is the representation of some entity that can be manipulated by a program
        </Text>

        <CodePane lang={'js'} source={source} margin={'2rem 0 0'} />
      </Slide>
    );
  }
}
