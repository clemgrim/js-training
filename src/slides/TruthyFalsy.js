import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class TruthyFalsy extends React.Component {
  render() {
    const source = require('raw-loader!../code/truthy-falsy.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Truthy / Falsy
        </Heading>

        <Text textColor={'primary'} fit>
          A value is truthy if it equals true, falsy if equals false.
        </Text>

        <CodePane lang={'js'} source={source} margin={'2rem 0 0'} />
      </Slide>
    );
  }
}
