import React from 'react';
import { Heading, Slide, CodePane } from 'spectacle';

export default class NumbersOperations extends React.Component {
  render() {
    const source = require('raw-loader!../code/numbers-operations.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Operations
        </Heading>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
