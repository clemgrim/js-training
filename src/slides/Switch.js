import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import CodePane from "spectacle/es/components/code-pane";

export default class Switch extends React.Component {
  render() {
    const source = require('raw-loader!../code/switch.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Switch cases
        </Heading>

        <Text textColor={'primary'} fit margin={'20px 0'}>
          We use the <strong>switch</strong> statement when we want to test different cases for one value.
          <br />
          It's usually cleaner than several if.
        </Text>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
