import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class StringsMultiline extends React.Component {
  render() {
    const source = require('raw-loader!../code/strings-multiline.txt');
    const color = {color: '#c311aa'};

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Strings - Multiline
        </Heading>

        <CodePane lang={'js'} source={source} margin={'20px 0'} />

        <Text textColor={'primary'} fit>
          It is easier to handle multiline strings with <strong style={color}>`</strong> delimiter
        </Text>
      </Slide>
    );
  }
}
