import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import CodePane from "spectacle/es/components/code-pane";

export default class IfElse extends React.Component {
  render() {
    const source = require('raw-loader!../code/if-else.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          If / Else
        </Heading>

        <Text textColor={'primary'} fit margin={'20px 0'}>
          <strong>If</strong> and <strong>else</strong> statements are used
          to define a block that will be executed when the condition is met
        </Text>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
