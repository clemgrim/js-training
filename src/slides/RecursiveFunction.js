import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class RecursiveFunctions extends React.Component {
  render() {
    const source = require('raw-loader!../code/recursive-functions.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1} fit>
          Recursive functions
        </Heading>

        <Text textColor={'primary'} fit>
          A recursive function is a function calling itself recursively.
        </Text>

        <CodePane lang={'js'} source={source} margin={'20px 0'}/>
      </Slide>
    );
  }
}
