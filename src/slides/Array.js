import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import CodePane from "spectacle/es/components/code-pane";

export default class Array extends React.Component {
  render() {
    const source = require('raw-loader!../code/array.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Array (lists)
        </Heading>

        <Text textColor={'primary'} fit margin={'20px 0'}>
          Arrays are a subtype of Objects, used to store a list of values.
        </Text>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
