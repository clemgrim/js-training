import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class StringsConcatenation extends React.Component {
  render() {
    const source = require('raw-loader!../code/strings-concatenation.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Strings - Concat
        </Heading>

        <Text textColor={'primary'} fit>
          Concatenation is the operation of joining two strings together.
        </Text>

        <CodePane lang={'js'} source={source} margin={'20px 0'} />
      </Slide>
    );
  }
}
