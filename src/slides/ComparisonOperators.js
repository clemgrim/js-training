import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class ComparisonOperators extends React.Component {
  render() {
    const source = require('raw-loader!../code/comparison-operators.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1} fit>
          Comparison Operators
        </Heading>

        <Text textColor={'primary'} fit>
          Comparison operators are used in logical statements to determine equality or difference between two values.
        </Text>

        <CodePane lang={'js'} source={source} margin={'2rem 0 0'} />
      </Slide>
    );
  }
}
