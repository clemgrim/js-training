import React from 'react';
import { Heading, Slide, CodePane } from 'spectacle';

export default class ArrowFunctionsVocabulary extends React.Component {
  render() {
    const source = require('raw-loader!../code/arrow-functions-vocabulary.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Vocabulary II
        </Heading>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
