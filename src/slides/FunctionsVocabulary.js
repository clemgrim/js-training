import React from 'react';
import { Heading, Slide, CodePane } from 'spectacle';

export default class FunctionsVocabulary extends React.Component {
  render() {
    const source = require('raw-loader!../code/functions-vocabulary.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Vocabulary
        </Heading>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
