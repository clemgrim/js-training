import React from 'react';
import { Heading, Slide, Image } from 'spectacle';

export default class Evolution extends React.Component {
  render() {
    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Evolution
        </Heading>

        <Image src={require('../assets/ecma-evo.png')} />
      </Slide>
    );
  }
}
