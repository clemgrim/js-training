import React from 'react';
import { Heading, Slide, CodePane } from 'spectacle';

export default class StringCharacters extends React.Component {
  render() {
    const source = require('raw-loader!../code/string-characters.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          String characters
        </Heading>

        <CodePane lang={'js'} source={source} margin={'20px 0'} />
      </Slide>
    );
  }
}
