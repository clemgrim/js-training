import React from 'react';
import { Heading, Slide, CodePane } from 'spectacle';

export default class Numbers extends React.Component {
  render() {
    const source = require('raw-loader!../code/numbers.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          Numbers
        </Heading>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
