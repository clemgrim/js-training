import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class RunInBrowser extends React.Component {
  render() {
    const source = require('raw-loader!../code/js-in-browser.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1} fit>
          Run JavaScript in browser
        </Heading>

        <Text textColor={'primary'} fit>
          We use the HTML element <strong>script</strong> to run JS in the browser.
        </Text>

        <Text textColor={'primary'} fit>
          We can write JS directly in the HTML (inside the script element) or we can include a file via the src attribute.
        </Text>

        <CodePane lang={'html'} source={source} margin={'2rem 0 0'} />
      </Slide>
    );
  }
}
