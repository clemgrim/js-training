import React from 'react';
import { Heading, Slide, CodePane, Text } from 'spectacle';

export default class NullUndefined extends React.Component {
  render() {
    const source = require('raw-loader!../code/null-undefined.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1} fit>
          Null and Undefined
        </Heading>

        <Text textColor={'primary'} fit>
          The value of any non-initialized variable is undefined.
        </Text>

        <CodePane lang={'js'} source={source} margin={'2rem 0 0'} />
      </Slide>
    );
  }
}
