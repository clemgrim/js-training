import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import CodePane from "spectacle/es/components/code-pane";

export default class ForLoop extends React.Component {
  render() {
    const source = require('raw-loader!../code/for-loop.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          For Loops
        </Heading>

        <Text textColor={'primary'} fit margin={'20px 0'}>
          We use the for loop when we want to repeat some code a specific number of times.
        </Text>

        <CodePane lang={'js'} source={source} />
      </Slide>
    );
  }
}
