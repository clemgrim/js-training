import React from 'react';
import { Heading, Slide, Image, CodePane, Text } from 'spectacle';

export default class Console extends React.Component {
  render() {
    const source = require('raw-loader!../code/console.txt');

    return (
      <Slide bgColor="secondary">
        <Heading size={1} lineHeight={1}>
          The console
        </Heading>

        <Text textColor={'primary'} fit>
          You can open the browser console with cmd+maj+c (Inspect element)
        </Text>

        <Text textColor={'primary'} fit>
          All the JS logs are printed in this console. You can also run JS in the console.
        </Text>

        <Image src={require('../assets/console.png')} width={600} margin={'2rem auto 0'} />

        <CodePane lang={'html'} source={source} margin={'2rem 0 0'} />
      </Slide>
    );
  }
}
