// Import React
import React from 'react';

// Import Spectacle Core tags
import { Deck } from 'spectacle';
import createTheme from 'spectacle/lib/themes/default';

// Require CSS
import 'normalize.css';
import 'prism-themes/themes/prism-darcula.css';

// Import slides
import Home from './slides/Home';
import Introduction from './slides/Introduction';
import Evolution from './slides/Evolution';
import RunInBrowser from './slides/RunInBrowser';
import Values from './slides/Values';
import ValueTypes from './slides/ValueTypes';
import Variables from './slides/Variables';
import Strings from './slides/Strings';
import StringsQuotes from './slides/StringsQuotes';
import StringsMultiline from './slides/StringsMultiline';
import StringsConcatenation from './slides/StringsConcatenation';
import Numbers from './slides/Numbers';
import NumbersOperations from './slides/NumbersOperations';
import NumbersFormat from './slides/NumbersFormat';
import Math from './slides/Math';
import Boolean from './slides/Boolean';
import ComparisonOperators from './slides/ComparisonOperators';
import LogicalOperators from './slides/LogicalOperators';
import TruthyFalsy from './slides/TruthyFalsy';
import NullUndefined from './slides/NullUndefined';
import Functions from './slides/Functions';
import FunctionsVocabulary from './slides/FunctionsVocabulary';
import ArrowFunctionsVocabulary from './slides/ArrowFunctionsVocabulary';
import ArrowFunctions from './slides/ArrowFunctions';
import Console from './slides/Console';
import IfElse from './slides/IfElse';
import ForLoop from './slides/ForLoop';
import StringCharacters from './slides/StringCharacters';
import StringFunctions from './slides/StringFunctions';
import RecursiveFunctions from './slides/RecursiveFunction';
import Switch from './slides/Switch';
import While from './slides/While';
import Array from './slides/Array';

// Create theme
const theme = createTheme(
  {
    primary: '#fff',
    secondary: '#333',
    tertiary: '#f7df1e',
  },
  {
    primary: 'Montserrat',
    secondary: 'Helvetica',
  }
);

theme.screen.components.heading.h1.fontSize = '6rem';
theme.screen.components.heading.h1.marginBottom = '2rem';
theme.screen.components.codePane.fontSize = '1rem';

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck
        transition={['zoom', 'slide']}
        transitionDuration={500}
        theme={theme}
      >
        <Home />
        <Introduction />
        <Evolution />
        <RunInBrowser />
        <Console />
        <Values />
        <ValueTypes />
        <Variables />
        <Strings />
        <StringsQuotes />
        <StringsMultiline />
        <StringsConcatenation />
        <StringCharacters />
        <StringFunctions />
        <Numbers />
        <NumbersOperations />
        <NumbersFormat />
        <Math />
        <Functions />
        <FunctionsVocabulary />
        <ArrowFunctions />
        <ArrowFunctionsVocabulary />
        <RecursiveFunctions />
        <NullUndefined />
        <Boolean />
        <ComparisonOperators />
        <LogicalOperators />
        <TruthyFalsy />
        <IfElse />
        <Switch />
        <ForLoop/>
        <While />
        <Array />
      </Deck>
    );
  }
}
